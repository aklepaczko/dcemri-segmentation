import numpy as np
from image_reader import read_nii_image
from main import make_paths
from os import listdir
from os.path import join, isfile
from itertools import product


def calculate_mean_signal_patient(patient_id: int, study_id: int) -> np.ndarray:
    paths = make_paths(patient_id, study_id)

    study_folder = paths.get('study_folder')

    image_files = [f for f in listdir(study_folder) if
                   (isfile(join(study_folder, f)) and (f.endswith('.nii') or f.endswith('.nii.gz')))]

    image_files = sorted(image_files)

    roi_folder = paths.get('roi_folder')

    roi_right = join(roi_folder, f'SG2_FF{patient_id:02d}_{study_id:d}_intensity-segmentations-right-label.nii')
    roi_left = join(roi_folder, f'SG2_FF{patient_id:02d}_{study_id:d}_intensity-segmentations-left-label.nii')

    segmentation_right, _ = read_nii_image(roi_right)
    segmentation_left, _ = read_nii_image(roi_left)
    left_cortex_roi = segmentation_left == 1
    right_cortex_roi = segmentation_right == 1
    left_kidney_roi = (segmentation_left == 1) | (segmentation_left == 2)
    right_kidney_roi = (segmentation_right == 1) | (segmentation_right == 2)
    left_medulla_roi = segmentation_left == 2
    right_medulla_roi = segmentation_right == 2
    left_pelvis_roi = segmentation_left == 3
    right_pelvis_roi = segmentation_right == 3

    num_frames = len(image_files)  # ok, I know, it's 74

    mean_signals = np.zeros((8, num_frames))

    for i, im_name in enumerate(image_files):
        dce_data, _ = read_nii_image(join(study_folder, im_name))
        mean_signals[0, i] = np.mean(dce_data[left_cortex_roi])
        mean_signals[1, i] = np.mean(dce_data[right_cortex_roi])
        mean_signals[2, i] = np.mean(dce_data[left_kidney_roi])
        mean_signals[3, i] = np.mean(dce_data[right_kidney_roi])
        mean_signals[4, i] = np.mean(dce_data[left_medulla_roi])
        mean_signals[5, i] = np.mean(dce_data[right_medulla_roi])
        mean_signals[6, i] = np.mean(dce_data[left_pelvis_roi])
        mean_signals[7, i] = np.mean(dce_data[right_pelvis_roi])

    return mean_signals


def save_means_array(patient_id: int, study_id: int, array_to_save: np.ndarray):
    paths = make_paths(patient_id, study_id)
    arrays_path = paths.get('arrays_folder')
    array_filename = join(arrays_path, f'SG2_FF{patient_id:02d}_{study_id:d}_SignalMeans.npy')
    np.save(array_filename, array_to_save)


if __name__ == '__main__':
    patients = set(range(1, 11))
    examinations = [1, 2]
    for subject, examination in list(product(patients, examinations)):
        mean_signals = calculate_mean_signal_patient(subject, examination)
        save_means_array(subject, examination, mean_signals)
