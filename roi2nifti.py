#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 12:44:20 2018

@author: artur
"""

from PIL import Image
from os import listdir
from os.path import isfile, join, exists
import numpy as np
import nibabel as nii
from main import make_paths
from image_reader import read_nii_image
from itertools import product


def get_frame(patient_id: int, study_id: int) -> int:
    frame_choices = {(1, 1): 13, (1, 2): 16,
                     (2, 1): 12, (2, 2): 13,
                     (3, 1): 10, (3, 2): 10,
                     (4, 1): 13, (4, 2): 11,
                     (5, 1): 12, (5, 2): 12,
                     (6, 1): 12, (6, 2): 12,
                     (7, 1): 12, (7, 2): 12,
                     (8, 1): 12, (8, 2): 12,
                     (9, 1): 12, (9, 2): 12,
                     (10, 1): 12, (10, 2): 12}
    return frame_choices.get((patient_id, study_id))


def create_kidney_mask_study(patient_id: int, study_id: int):
    num_slices = 30
    rows = 192
    cols = 192

    cortex_label = 6
    medulla_label = 1
    pelvis_label = 224
    background_label = 292

    kidney = np.zeros((rows, cols, num_slices), dtype='uint16')

    paths = make_paths(patient_id, study_id)
    manual_roi_folder = paths.get('manual_folder')

    roi_files = [f for f in listdir(manual_roi_folder) if
                 (isfile(join(manual_roi_folder, f)) and f.endswith('.roi'))]

    frame_id = get_frame(patient_id, study_id)

    def is_chosen_frame(filename):
        end_idx = str(filename).rindex('Slice')
        frame_number = int(filename[end_idx-2:end_idx])
        slice_number = int(filename[-6:-4]) - 1
        return (frame_number == frame_id) and (slice_number in range(14, 19))

    roi_files_frame = list(filter(is_chosen_frame, roi_files))

    for roi_file in roi_files_frame:
        slice_id = int(roi_file[-6:-4])-1
        roi = Image.open(join(manual_roi_folder, roi_file))
        slice_array = np.zeros((rows, cols), dtype='uint16')
        # left medulla
        roi.seek(1)
        medulla_left_mask = np.transpose(np.array(roi, dtype=bool))
        # right medulla
        roi.seek(4)
        medulla_right_mask = np.transpose(np.array(roi, dtype=bool))
        slice_array[medulla_left_mask | medulla_right_mask] = medulla_label

        # left pelvis
        roi.seek(2)
        pelvis_left_mask = np.transpose(np.array(roi, dtype=bool))
        # right pelvis
        roi.seek(5)
        pelvis_right_mask = np.transpose(np.array(roi, dtype=bool))
        slice_array[pelvis_left_mask | pelvis_right_mask] = pelvis_label

        # left cortex
        roi.seek(6)
        cortex_left_mask = np.transpose(np.array(roi, dtype=bool))
        # right cortex
        roi.seek(7)
        cortex_right_mask = np.transpose(np.array(roi, dtype=bool))
        slice_array[cortex_left_mask | cortex_right_mask] = cortex_label

        kidney[:, :, slice_id] = slice_array

        output_dir = paths.get('roi_folder')
        legacy_roi_name = f'SG2_FF{patient_id:02d}_{study_id:d}_TrainingROIs.nii'
        legacy_roi, _ = read_nii_image(join(output_dir, legacy_roi_name))
        kidney[legacy_roi == background_label] = background_label

        image_folder = paths.get('study_folder')
        image_filename = f'SG2_FF{patient_id:02d}_{study_id:d}_Reg{frame_id}.nii'
        if exists(join(image_folder, image_filename)):
            _, affine = read_nii_image(join(image_folder, image_filename))
        else:
            image_filename += '.gz'
            _, affine = read_nii_image(join(image_folder, image_filename))

        nii_img = nii.Nifti1Image(kidney, affine)
        output_name = f'SG2_FF{patient_id:02d}_{study_id:d}_TrainingTiffROIs.nii'
        nii.save(nii_img, join(output_dir, output_name))


if __name__ == '__main__':
    patients = set(range(1, 11))
    examinations = [1, 2]
    for subject, examination in list(product(patients, examinations)):
        create_kidney_mask_study(subject, examination)
        print(f'Patient {subject:d}, study {examination:d} done.')
