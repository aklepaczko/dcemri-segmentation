# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 08:36:27 2018

Upgraded on Wed Jan 20 10:53:00 2021

@author: Artur Klepaczko (C) Lodz University of Technology

"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

iohexol = [107, 98, 90, 93, 94, 103, 112, 96, 119, 112]
auto_MR1 = [121,92,83,121,85,86,154,85,140,132]
auto_MR2 = [112,95,110,113,99,108,144,92,150,132]

auto = [auto_MR1, auto_MR2]

manual_MR1 = [110, 119, 66, 103, 53, 65, 137, 72, 124, 112]
manual_MR2 = [144, 82, 136, 140, 90, 122, 154, 86, 149, 152]

manual = [manual_MR1, manual_MR2]

def generate_BA_plot(ref_data, test_data, 
                     plot_title, ref_name, test_name):
    means = (np.array(ref_data) + np.array(test_data)) /2
    difs = np.array(ref_data) - np.array(test_data)

    md = np.mean(difs)
    sd = np.std(difs)

    plt.figure(dpi=150)
    plt.scatter(means, difs)
    
    kappa = 1.96*sd
    plt.axhline(md, color='r', label='Mean difference, $\mu_d$')
    plt.axhline(md + kappa, color='r', linestyle='dashed',
                label='95% limit of agreement, $\kappa$')
    plt.axhline(md - kappa, color='r', linestyle='dashed')

    n = len(means)
    T = stats.t.ppf(0.95, n-1)

    CI = 1.96*T*sd/(n**0.5)
    CI_up = md + kappa + CI
    CI_bottom = md - kappa - CI

    plt.axhline(CI_up, color='g', linestyle='dotted', label='95% confidence interval, $CI$')
    plt.axhline(CI_bottom, color='g', linestyle='dotted')
    
    plt.title(plot_title)
    stats_str = '$\mu_d=%1.2f, \kappa=\mu_d\pm %1.2f, CI=\kappa\pm %1.2f $' % (md, kappa, CI)
    plt.xlabel(f'({ref_name:s}+{test_name:s})/2' + ' $[\mathrm{ml/min/1.73\ m}^2]$\n\n'\
               + stats_str)
    plt.ylabel(f'{ref_name:s}-{test_name:s}\n'+ '$[\mathrm{ml/min/1.73\ m}^2]$')
    plt.tight_layout()
    plt.legend(loc='upper right')


generate_BA_plot(iohexol, manual_MR1,
                 'Manual segmentation, MR session 1',
                 'Iohexol_GFR',
                 'ImageDerived_GFR')

generate_BA_plot(iohexol, manual_MR2,
                 'Manual segmentation, MR session 2',
                 'Iohexol_GFR',
                 'ImageDerived_GFR')

generate_BA_plot(iohexol, auto_MR1,
                 'Automatic segmentation, MR session 1',
                 'Iohexol_GFR',
                 'ImageDerived_GFR')

generate_BA_plot(iohexol, auto_MR2,
                 'Automatic segmentation, MR session 2',
                 'Iohexol_GFR',
                 'ImageDerived_GFR')