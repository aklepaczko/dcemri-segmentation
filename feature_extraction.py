import numpy as np
from utils import make_paths
from os import listdir
from os.path import isfile, join
from image_reader import read_nii_image
from SemiQuantitative.signal_feature_extractor import get_wavelet_features, get_wavelet_decomposition
from signal_feature_extractor import interpolate
from pandas import DataFrame, read_csv
from typing import Tuple, List


wavelet = 'signal'


def extract_dwt_coefficients(signals: np.ndarray) -> Tuple[np.ndarray, List]:
    dwt_coeffs = []
    for signal in signals[:-1]:
        dwt1, dwt2, dwt3, _ = get_wavelet_decomposition(signal, wavelet_fun=wavelet)
        dwt_coeffs.append([*dwt1, *dwt2, *dwt3, 0])
    dwt1, dwt2, dwt3, feature_names = get_wavelet_decomposition(signals[-1, :], wavelet_fun=wavelet)
    dwt_coeffs.append([*dwt1, *dwt2, *dwt3, 0])
    return np.array(dwt_coeffs), feature_names


def extract_dwt_with_interpolation_coefficients(signals: np.ndarray) -> Tuple[np.ndarray, List]:
    return extract_dwt_coefficients(interpolate(signals, 2.3, 0.4))


def extract_dwt_features(signals: np.ndarray) -> Tuple[np.ndarray, List]:
    dwt_features = []
    for signal in signals[:-1]:
        dwt1, dwt2, dwt3, _ = get_wavelet_features(signal, wavelet_fun=wavelet)
        dwt_features.append([*dwt1, *dwt2, *dwt3, 0])
    dwt1, dwt2, dwt3, feature_names = get_wavelet_features(signals[-1, :], wavelet_fun=wavelet)
    dwt_features.append([*dwt1, *dwt2, *dwt3, 0])
    return np.array(dwt_features), feature_names


def extract_dwt_with_interpolation_features(signals: np.ndarray) -> Tuple[np.ndarray, List]:
    return extract_dwt_features(interpolate(signals, 2.3, 0.4))


def extract_intensity_features(signals: np.ndarray) -> Tuple[np.ndarray, List]:
    intensity_features = np.append(signals, -1*np.ones((signals.shape[0], 1)), axis=1)
    return intensity_features, list(range(signals.shape[1]))


def make_extractor(extractor_type: str) -> callable(np.ndarray):
    extractors = {'intensity': extract_intensity_features,
                  'dwt': extract_dwt_features,
                  'dwt_interp': extract_dwt_with_interpolation_features,
                  'dwt_coeff': extract_dwt_coefficients,
                  'dwt_coeff_interp': extract_dwt_with_interpolation_coefficients}
    return extractors.get(extractor_type, 'dwt')


def make_training_data_study(patient_id, study_id, extractor_type, save_features=False):

    paths = make_paths(patient_id, study_id)
    study_folder = paths.get('study_folder')

    image_files = [f for f in listdir(study_folder) if
                   (isfile(join(study_folder, f)) and (f.endswith('.nii') or f.endswith('.nii.gz')))]

    image_files = sorted(image_files)

    roi_folder = paths.get('roi_folder')

    roi_filename = join(roi_folder, f'SG2_FF{patient_id:02d}_{study_id:d}_TrainingROIs.nii')
    training_roi, _ = read_nii_image(roi_filename)

    num_frames = len(image_files)  # ok, I know, it's 74
    cortex_roi = training_roi == 6
    medulla_roi = training_roi == 1
    pelvis_roi = training_roi == 224
    background_roi = training_roi == 292

    cortex_data = np.zeros((np.sum(cortex_roi), num_frames), dtype='uint16')
    medulla_data = np.zeros((np.sum(medulla_roi), num_frames), dtype='uint16')
    pelvis_data = np.zeros((np.sum(pelvis_roi), num_frames), dtype='uint16')
    background_data = np.zeros((np.sum(background_roi), num_frames), dtype='uint16')

    for i, im_name in enumerate(image_files[:num_frames]):
        dce_data, _ = read_nii_image(join(study_folder, im_name))
        cortex_data[:, i] = dce_data[cortex_roi]
        medulla_data[:, i] = dce_data[medulla_roi]
        pelvis_data[:, i] = dce_data[pelvis_roi]
        background_data[:, i] = dce_data[background_roi]

    extractor = make_extractor(extractor_type)
    cortex_features, _ = extractor(cortex_data)
    cortex_features[:, -1] = 1

    medulla_features, _ = extractor(medulla_data)
    medulla_features[:, -1] = 2

    pelvis_features, _ = extractor(pelvis_data)
    pelvis_features[:, -1] = 3

    background_features, feature_names = extractor(background_data)
    background_features[:, -1] = 4
    feature_names.append('Roi_type')

    if save_features:

        arrays_folder = paths.get('arrays_folder')
        features_filename = f'SG2_FF{patient_id:02d}_{study_id:d}_features-{extractor_type}_{wavelet:s}_TrainROIs.csv'
        df = DataFrame(data=np.concatenate((cortex_features,
                                            medulla_features,
                                            pelvis_features,
                                            background_features),
                                           axis=0),
                       columns=feature_names)
        df.to_csv(join(arrays_folder, features_filename), sep=',')
        print(f'Saved data file: {features_filename:s}')
    return cortex_features, medulla_features, pelvis_features, background_features


def make_training_data_cohort(patient_ids, study_ids, *, extractor_type='dwt', save=False):
    c = []
    m = []
    p = []
    # b = []
    for patient_id in patient_ids:
        for study_id in study_ids:
            # print(f'Perform feature extraction for patient {patient_id:d} and study {study_id:d}')
            paths = make_paths(patient_id, study_id)
            arrays_folder = paths.get('arrays_folder')
            df = read_csv(join(arrays_folder,
                               f'SG2_FF{patient_id:02d}_{study_id:d}_features-{extractor_type:s}_{wavelet}_TrainROIs.csv'),
                          sep=',')

            df.dropna(inplace=True)
            x = df.to_numpy()
            labels = x[:, -1]
            cortex = x[labels == 1, 1:]
            medulla = x[labels == 2, 1:]
            pelvis = x[labels == 3, 1:]
            # background = x[labels == 4, 1:]
            c.append(cortex)
            m.append(medulla)
            p.append(pelvis)
            # b.append(background)
    cortex_all = np.concatenate(c)
    medulla_all = np.concatenate(m)
    pelvis_all = np.concatenate(p)
    # background_all = np.concatenate(b)
    return np.concatenate((cortex_all, medulla_all, pelvis_all))#, background_all))

