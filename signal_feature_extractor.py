import numpy as np
from scipy import interpolate as intrp


def interpolate(signals: np.ndarray, dt: float, dt_interp: float) -> np.ndarray:
    """
    Interpolates input signal to the dt_interp temporal resolution from the base dt resolution
    :param dt: float
        Input temporal resolution, seconds
    :param signals: 2D array of float
        Signals to interpolate. Each column represents a successive time step. Every row is a separate voxel.
    :param dt_interp: float
        Target temporal resolution, seconds
    :return: array of float
        Interpolated signal
    """
    num_signals, num_time_frames = signals.shape

    time_window = np.arange(0, dt * num_time_frames, dt)

    time_window_interpolated = np.linspace(0, time_window[-1], int(round(time_window[-1] / dt_interp)))

    signals_interp = []
    for signal in signals:
        tck = intrp.splrep(time_window, signal, s=0)
        signals_interp.append(intrp.splev(time_window_interpolated, tck, der=0))

    return np.array(signals_interp)
