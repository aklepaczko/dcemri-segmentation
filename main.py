import numpy as np
from xgboost.sklearn import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.preprocessing import scale
from SemiQuantitative.signal_feature_extractor import get_semi_quantitative_features, get_wavelet_features
from feature_extraction import make_training_data_cohort, make_extractor, make_training_data_study
from image_feature_extractor import get_glcm_feature_array
import matplotlib.pyplot as plt
from image_reader import read_nii_image
from os.path import join, isfile
from os import listdir
from nibabel import Nifti1Image, save
from itertools import product
from utils import make_paths, get_frame


def save_segmentations(patient_id: int, study_id: int, classifications: np.ndarray, mask_label: str, extractor_type: str):
    paths = make_paths(patient_id, study_id)

    mask_folder = paths.get('mask_folder')
    frame_id = get_frame(patient_id, study_id)

    roi_filename = join(mask_folder,
                        f'SG2_FF{patient_id:02d}_{study_id:d}_Frame{frame_id:02d}_{mask_label}-kidney-label.nii')

    kidney_mask, affine = read_nii_image(roi_filename)
    kidney_roi = kidney_mask == 1

    output_image = np.zeros_like(kidney_mask)
    output_image[kidney_roi] = classifications
    nii_img = Nifti1Image(output_image, affine)
    roi_folder = paths.get('roi_folder')
    save(nii_img, join(roi_folder, f'SG2_FF{patient_id:02d}_{study_id:d}_{extractor_type:s}-segmentations-{mask_label}-label.nii'))


def make_segmentation_data_study(patient_id, study_id, *, extractor_type):

    paths = make_paths(patient_id, study_id)

    study_folder = paths.get('study_folder')

    image_files = [f for f in listdir(study_folder) if
                   (isfile(join(study_folder, f)) and (f.endswith('.nii') or f.endswith('.nii.gz')))]

    image_files = sorted(image_files)
    roi_folder = paths.get('mask_folder')

    frame_id = get_frame(patient_id, study_id)

    # left kidney
    roi_filename = join(roi_folder,
                        f'SG2_FF{patient_id:02d}_{study_id:d}_Frame{frame_id:02d}_left-kidney-label.nii')
    kidney_mask, _ = read_nii_image(roi_filename)
    left_kidney_roi = kidney_mask == 1  # 223

    # right kidney
    roi_filename = join(roi_folder,
                        f'SG2_FF{patient_id:02d}_{study_id:d}_Frame{frame_id:02d}_right-kidney-label.nii')
    kidney_mask, _ = read_nii_image(roi_filename)

    right_kidney_roi = kidney_mask == 1  # 222

    num_frames = len(image_files)  # ok, I know, it's 74

    image_left_data = np.zeros((np.sum(left_kidney_roi), num_frames), dtype='uint16')
    image_right_data = np.zeros((np.sum(right_kidney_roi), num_frames), dtype='uint16')

    for i, im_name in enumerate(image_files[:num_frames]):
        dce_data, _ = read_nii_image(join(study_folder, im_name))
        image_left_data[:, i] = dce_data[left_kidney_roi]
        image_right_data[:, i] = dce_data[right_kidney_roi]

    extractor = make_extractor(extractor_type)
    left_kidney_features, _ = extractor(image_left_data)
    left_kidney_features[:, -1] = -1

    right_kidney_features, _ = extractor(image_right_data)
    right_kidney_features[:, -1] = -1

    return right_kidney_features, left_kidney_features


def save_segmentation_data_study_roi(patient_id, study_id, *, extractor_type):

    paths = make_paths(patient_id, study_id)

    study_folder = paths.get('study_folder')

    image_files = [f for f in listdir(study_folder) if
                   (isfile(join(study_folder, f)) and (f.endswith('.nii') or f.endswith('.nii.gz')))]

    image_files = sorted(image_files)
    roi_folder = paths.get('mask_folder')

    frame_id = get_frame(patient_id, study_id)

    # left kidney
    roi_filename = join(roi_folder,
                        f'SG2_FF{patient_id:02d}_{study_id:d}_Frame{frame_id:02d}_left-kidney-label.nii')
    kidney_mask, _ = read_nii_image(roi_filename)
    left_kidney_roi = kidney_mask == 1

    # right kidney
    roi_filename = join(roi_folder,
                        f'SG2_FF{patient_id:02d}_{study_id:d}_Frame{frame_id:02d}_right-kidney-label.nii')
    kidney_mask, _ = read_nii_image(roi_filename)

    right_kidney_roi = kidney_mask == 1

    num_frames = len(image_files)  # ok, I know, it's 74

    image_left_data = np.zeros((np.sum(left_kidney_roi), num_frames), dtype='uint16')
    image_right_data = np.zeros((np.sum(right_kidney_roi), num_frames), dtype='uint16')

    for i, im_name in enumerate(image_files[:num_frames]):
        dce_data, _ = read_nii_image(join(study_folder, im_name))
        image_left_data[:, i] = dce_data[left_kidney_roi]
        image_right_data[:, i] = dce_data[right_kidney_roi]

    extractor = make_extractor(extractor_type)
    left_kidney_features, feature_names = extractor(image_left_data)
    left_kidney_features[:, -1] = -1
    feature_names.append('Roi_type')
    arrays_folder = paths.get('arrays_folder')
    wavelet = 'signal'
    features_filename = f'SG2_FF{patient_id:02d}_{study_id:d}_features-{extractor_type}_{wavelet:s}_left.csv'
    from pandas import DataFrame
    df = DataFrame(data=left_kidney_features, columns=feature_names)
    df.to_csv(join(arrays_folder, features_filename), sep=',')

    right_kidney_features, _ = extractor(image_right_data)
    right_kidney_features[:, -1] = -1
    features_filename = f'SG2_FF{patient_id:02d}_{study_id:d}_features-{extractor_type}_{wavelet:s}_right.csv'
    df = DataFrame(data=right_kidney_features, columns=feature_names)
    df.to_csv(join(arrays_folder, features_filename), sep=',')


def save_test_data_study_roi(patient_id, study_id, *, roi_file, extractor_type):

    paths = make_paths(patient_id, study_id)

    study_folder = paths.get('study_folder')

    image_files = [f for f in listdir(study_folder) if
                   (isfile(join(study_folder, f)) and (f.endswith('.nii') or f.endswith('.nii.gz')))]

    image_files = sorted(image_files)
    roi_folder = paths.get('roi_folder')

    roi_filepath = join(roi_folder, roi_file)

    test_mask, _ = read_nii_image(roi_filepath)
    cortex_roi = test_mask == 1
    medulla_roi = test_mask == 2
    pelvis_roi = test_mask == 3

    num_frames = len(image_files)  # ok, I know, it's 74

    cortex_data = np.zeros((np.sum(cortex_roi), num_frames), dtype='uint16')
    medulla_data = np.zeros((np.sum(medulla_roi), num_frames), dtype='uint16')
    pelvis_data = np.zeros((np.sum(pelvis_roi), num_frames), dtype='uint16')

    for i, im_name in enumerate(image_files[:num_frames]):
        dce_data, _ = read_nii_image(join(study_folder, im_name))
        cortex_data[:, i] = dce_data[cortex_roi]
        medulla_data[:, i] = dce_data[medulla_roi]
        pelvis_data[:, i] = dce_data[pelvis_roi]

    extractor = make_extractor(extractor_type)
    cortex_features, feature_names = extractor(cortex_data)
    cortex_features[:, -1] = 1

    medulla_features, _ = extractor(medulla_data)
    medulla_features[:, -1] = 2

    pelvis_features, _ = extractor(pelvis_data)
    pelvis_features[:, -1] = 3

    feature_names.append('Roi_type')
    arrays_folder = paths.get('arrays_folder')
    wavelet = 'db4'
    features_filename = f'SG2_FF{patient_id:02d}_{study_id:d}_features-{extractor_type}_{wavelet:s}_tiramisu.csv'
    from pandas import DataFrame
    df = DataFrame(data=np.concatenate((cortex_features,
                                        medulla_features,
                                        pelvis_features), axis=0),
                   columns=feature_names)
    df.to_csv(join(arrays_folder, features_filename), sep=',')


def run_semi_quantitative(filename, side, roi):
    signals = np.load(filename)

    curve_ids = {('left', 'cortex'): 0,
                 ('right', 'cortex'): 1,
                 ('left', 'whole_kidney'): 2,
                 ('right', 'whole_kidney'): 3}

    signal = signals[curve_ids.get((side, roi)), :]
    plt.figure()
    plt.plot(signal)
    bat, peak, auc = get_semi_quantitative_features(signal)
    plt.axvline(bat, color='gray', linestyle='--')
    plt.axvline(peak, color='gray', linestyle='--')
    plt.axvline(auc, color='gray', linestyle='--')
    plt.show()


def show_image(filename, slice_no):
    img, _ = read_nii_image(filename)
    plt.figure(figsize=(3, 3))
    plt.imshow(np.fliplr(np.rot90(img[:, :, slice_no])), cmap=plt.cm.gray)
    plt.axis('off')
    plt.tight_layout()
    plt.show()


def extract_texture_features_mask(img_filename, roi_filename):
    img, _ = read_nii_image(img_filename)
    mask, _ = read_nii_image(roi_filename)
    features = get_glcm_feature_array(img, mask)
    return features


def make_xboost_estimator():
    xgb_classifier = XGBClassifier(
        learning_rate=0.5,
        n_estimators=50,
        max_depth=3,
        min_child_weight=1,
        objective='multi:softmax',
        n_jobs=8,
        scale_pos_weight=1,
        reg_lambda=0.9,
        reg_alpha=0.1,
        subsample=0.5
    )
    opts = {'eval_metric': 'merror'}
    return xgb_classifier, opts


def make_svm_estimator():
    svm_classifier = SVC(kernel='rbf', gamma='auto')
    opts = {}
    return svm_classifier, opts


def make_estimator(classifier_type: str):
    estimators = {'xgboost': make_xboost_estimator,
                  'svm': make_svm_estimator}
    estimator_maker = estimators.get(classifier_type, 'svm')
    return estimator_maker()


class DCEClassifier:
    __slots__ = ('_x_mean', '_x_std', '_classifier', '_training_data')

    def __init__(self, training_data):
        self._training_data = training_data
        self._classifier = None
        self._x_mean = None
        self._x_std = None

    def build_classifier(self, classifier, opts):
        x = self._training_data[:, :-1]
        y = self._training_data[:, -1]
        self._x_mean = np.mean(x, axis=0)
        self._x_std = np.std(x, axis=0)
        x_scaled = scale(x)
        x_train, _, y_train, _ = train_test_split(x_scaled, y, test_size=0.85)
        classifier.fit(x_train, y_train, **opts)
        self._classifier = classifier

    def make_predictions(self, unlabelled_data):
        x = (unlabelled_data[:, :-1] - self._x_mean) / self._x_std
        # probabilities = self._classifier.predict_proba(x)
        hard_preds = self._classifier.predict(x)
        # for i, prediction in enumerate(hard_preds):
        #     if probabilities[i, int(prediction)-1] < 0.6:
        #         hard_preds[i] = 0
        return hard_preds


def run_segmentation(patient: int, study: int, train_subjects: list, train_studies: list):
    feature_extractor = 'intensity'
    estimator, opts = make_estimator('svm')
    dataset = make_training_data_cohort(train_subjects, train_studies, extractor_type=feature_extractor)
    dce = DCEClassifier(dataset)
    dce.build_classifier(estimator, opts)
    image_data_right, image_data_left = make_segmentation_data_study(patient, study, extractor_type=feature_extractor)
    right_classifications = dce.make_predictions(image_data_right)
    save_segmentations(patient, study, right_classifications, 'right', feature_extractor)
    left_classifications = dce.make_predictions(image_data_left)
    save_segmentations(patient, study, left_classifications, 'left', feature_extractor)
    print(f'Performed segmentation for patient {patient:d} and study {study:d}')


if __name__ == '__main__':
    patients = set(range(1, 11))
    examinations = [1, 2]
    for subject, examination in list(product(patients, examinations)):
        # make_training_data_study(subject, examination, 'intensity', True)
        # run_segmentation(subject, examination, list(patients - {subject}), examinations)
        # run_segmentation(subject, examination, [subject], [examination])
        save_segmentation_data_study_roi(subject, examination, extractor_type='intensity')
    # save_test_data_study_roi(1, 1, roi_file='SG2_FF01_1_TestROIs.nii',
    #                          extractor_type='dwt_coeff_interp')
    # path = r'C:\Users\Artur Klepaczko\Documents\Science\KidneysData\BergenImagesBSpline\SG2_FF01\Vol_1'
    # filename = 'SG2_FF01_1_Reg15.nii'
    # show_image(join(path, filename), 16)
