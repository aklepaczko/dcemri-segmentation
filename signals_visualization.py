# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 23:10:02 2021

@author: Artur Klepaczko
"""
from os.path import join
import numpy as np

patient_id = 1
study_id = 1

root_folder = r'C:\Users\Artur Klepaczko\Documents\Science\KidneysData'

# root path to image frames
image_folder = join(root_folder, 'BergenImagesBSpline')

# path to patient data
patient_folder = join(image_folder, f'SG2_FF{patient_id:02d}')

arrays_path = join(patient_folder, f'Vol_{study_id}_Arrays')

signal_filename = join(arrays_path, f'SG2_FF{patient_id:02d}_{study_id:d}_SignalMeans.npy')

signals = np.load(signal_filename)

import matplotlib.pyplot as plt

cortex_color = 'c' #  '#80ae80'
medulla_color = 'r' #  '#f1d691'
pelvis_color = 'm' #  '#b17a65'

plt.plot(signals[0,:], color=cortex_color, label='cortex')
plt.plot(signals[4,:], color=medulla_color, label='medulla')
plt.plot(signals[6,:], color=pelvis_color, label='pelvis')
plt.xlabel('Time frame no.', fontsize=14)
plt.ylabel('Signal intensity [a.u.]', fontsize=14)
plt.legend(fontsize=14)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
