from skimage.feature import greycomatrix, greycoprops
import numpy as np


def get_glcm_feature_array(image, mask):
    image2 = (image / np.max(image) * 64).astype('uint8')
    xx, yy, zz = np.where(mask)
    w = 2
    glcm_feature_vectors = []
    for x, y, z in zip(xx, yy, zz):
        patch = image2[x-w:x+w, y-w:y+w, z]
        glcm = greycomatrix(patch, [1, 3], [0, np.pi/4, np.pi/2, 3*np.pi/4], 64, True, True)
        feature_vector = []
        feature_vector.extend(list(greycoprops(glcm, 'dissimilarity').flatten()))
        feature_vector.extend(list(greycoprops(glcm, 'contrast').flatten()))
        feature_vector.extend(list(greycoprops(glcm, 'correlation').flatten()))
        feature_vector.extend(list(greycoprops(glcm, 'energy').flatten()))
        feature_vector.extend(list(greycoprops(glcm, 'homogeneity').flatten()))
        feature_vector.extend(list(greycoprops(glcm, 'ASM').flatten()))
        glcm_feature_vectors.append(feature_vector)
    return np.array(glcm_feature_vectors)