from PkModeling import PkModeling2CFM
from main import make_paths
from os.path import join
import numpy as np
from itertools import product
from pandas import DataFrame
from collections import namedtuple


def run_pk_fitting(aif: np.ndarray, signal: np.ndarray, phase_frame: int, bat_frame: int) -> PkModeling2CFM:
    temporal_resolution_image = 2.3  # sec
    temporal_resolution_interp = 0.4  # sec

    flip_angle = 20
    TR = 2.36e-3
    T_10_blood = 1.4  # sec
    T_10_kid = 1.2  # sec
    relaxivity = 4.5

    mri_params = {'TR': TR,
                  'FA': flip_angle}

    pk_model = PkModeling2CFM(bat_frame=bat_frame, phase_frame=phase_frame,
                              dt=temporal_resolution_image, mri_params=mri_params,
                              T_10=T_10_blood, T_10_tissue=T_10_kid, relaxivity=relaxivity,
                              dt_interp=temporal_resolution_interp)

    pk_model.fit(aif, signal)

    return pk_model


def get_bat_study(patient_id: int, study_id: int) -> int:
    bat_look_up_table = {(1, 1): 10, (1, 2): 11,
                         (2, 1): 8, (2, 2): 9,
                         (3, 1): 7, (3, 2): 6,
                         (4, 1): 7, (4, 2): 7,
                         (5, 1): 9, (5, 2): 9,
                         (6, 1): 9, (6, 2): 8,
                         (7, 1): 8, (7, 2): 10,
                         (8, 1): 9, (8, 2): 12,
                         (9, 1): 11, (9, 2): 9,
                         (10, 1): 9, (10, 2): 9}
    return bat_look_up_table.get((patient_id, study_id))


def get_perfusion_phase_frame_study(patient_id: int, study_id: int) -> int:
    perfusion_choices = {(1, 1): 24, (1, 2): 24,
                         (2, 1): 20, (2, 2): 23,
                         (3, 1): 27, (3, 2): 19,
                         (4, 1): 24, (4, 2): 21,
                         (5, 1): 23, (5, 2): 23,
                         (6, 1): 24, (6, 2): 21,
                         (7, 1): 23, (7, 2): 25,
                         (8, 1): 25, (8, 2): 24,
                         (9, 1): 26, (9, 2): 25,
                         (10, 1): 24, (10, 2): 24}
    return perfusion_choices.get((patient_id, study_id))


def get_uptake_phase_frame_study(patient_id: int, study_id: int) -> int:
    perfusion_choices = {(1, 1): 45, (1, 2): 25,
                         (2, 1): 25, (2, 2): 40,
                         (3, 1): 45, (3, 2): 25,
                         (4, 1): 40, (4, 2): 25,
                         (5, 1): 40, (5, 2): 40,
                         (6, 1): 40, (6, 2): 40,
                         (7, 1): 40, (7, 2): 45,
                         (8, 1): 40,
                         (9, 2): 40,
                         (10, 2): 25}
    return perfusion_choices.get((patient_id, study_id), 40)


def estimate_gfr_study(patient_id: int, study_id: print, side: str, roi: str):
    paths = make_paths(patient_id, study_id)
    arrays_path = paths.get('arrays_folder')
    aif_path = join('..', 'ArraysCommon', 'aif-auto')
    signal_filename = join(arrays_path, f'SG2_FF{patient_id:02d}_{study_id:d}_SignalMeans.npy')
    aif_filename = join(aif_path, f'SG2_FF{patient_id:02d}_{study_id:d}_aif-auto_intensity.npy')

    aif = np.load(aif_filename)
    signals = np.load(signal_filename)

    curve_ids = {('left', 'cortex'): 0,
                 ('right', 'cortex'): 1,
                 ('left', 'kidney'): 2,
                 ('right', 'kidney'): 3}

    signal = signals[curve_ids.get((side, roi)), :]

    bat = get_bat_study(patient_id, study_id)
    phase_frame = get_uptake_phase_frame_study(patient_id, study_id)\
        if roi == 'kidney' else get_perfusion_phase_frame_study(patient_id, study_id)
    fitted_2cfm_model = run_pk_fitting(aif, signal, phase_frame, bat)

    volumes_path = join('..', 'ArraysCommon', 'volumes')
    volumes_filename = join(volumes_path, f'SG2_FF{patient_id:02d}_{study_id:d}_volumes.npy')
    volumes = np.load(volumes_filename)

    roi_volume = volumes[curve_ids.get((side, roi))]

    gfr = roi_volume * fitted_2cfm_model.Ktrans * 60 / 1000

    return gfr


def estimate_gfr_cohort(phase: str):
    patients = set(range(1, 11))
    examinations = [1, 2]
    results = []
    ResultTuple = namedtuple('ResultTuple', ['subject', 'examination', 'gfr_left', 'gfr_right', 'gfr_total'])
    for subject, examination in list(product(patients, examinations)):
        gfr_left = estimate_gfr_study(subject, examination, 'left', phase)
        gfr_right = estimate_gfr_study(subject, examination, 'right', phase)
        results.append(ResultTuple(subject, examination, gfr_left, gfr_right, gfr_left + gfr_right))
    return results


if __name__ == '__main__':
    phase = 'kidney'
    feature_type = 'intensity'
    gfr_estimates = estimate_gfr_cohort(phase)
    gfr_df = DataFrame()
    gfr_df['Subject'] = np.array([x.subject for x in gfr_estimates])
    gfr_df['Examination'] = np.array([x.examination for x in gfr_estimates])
    gfr_df['GFR left'] = np.array([x.gfr_left for x in gfr_estimates])
    gfr_df['GFR right'] = np.array([x.gfr_right for x in gfr_estimates])
    gfr_df['GFR total'] = np.array([x.gfr_total for x in gfr_estimates])
    report_filename = join('..', 'ArraysCommon', f'report-{phase:s}-{feature_type:s}.csv')
    gfr_df.to_csv(report_filename, sep=';', decimal=',')
