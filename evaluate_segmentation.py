# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 22:57:31 2021

@author: Artur Klepaczko
"""

# Evaluate coarse segmentation

from sklearn.metrics import jaccard_score
from nibabel import load
from os.path import join
import numpy as np


root = r'C:\Users\Artur Klepaczko\Documents\Science\KidneysData'
segments_folder = join(root, 'BergenAnnotations')

subject = 1
volume = 1
side = 'left'

patient_folder = join(segments_folder,
                      f'SG2_FF{subject:02d}',
                      f'Vol_{volume:d}_Annotations')

gt_name = f'SG2_FF{subject:02d}_{volume:d}_manual-{side:s}-label.nii'
gt_roi = load(join(patient_folder, gt_name)).get_fdata()

seg_name = f'SG2_FF{subject:02d}_{volume:d}_intensity-segmentations-{side:s}-label.nii'
seg_roi = load(join(patient_folder, seg_name)).get_fdata()

similarity = jaccard_score(gt_roi.flatten(), seg_roi.flatten(), average=None)
print(similarity)

cortex = [0.84, 0.87, 0.98, 0.99,
          0.99, 0.96, 0.91, 0.93,
          0.87, 0.93, 0.95, 0.98,
          0.98, 0.99, 1.00, 0.94,
          0.97, 0.96, 0.99, 0.99]

medulla = [0.94, 0.95, 0.85, 0.87,
           0.90, 0.92, 0.99, 0.98,
           0.96, 0.96, 0.98, 0.99,
           0.99, 0.99, 0.97, 0.99,
           0.99, 0.98, 0.95, 0.92]

pelvis = [1.00, 0.97, 1.00, 0.99,
          1.00, 0.98, 0.82, 0.88,
          0.96, 0.96, 1.00, 0.93,
          1.00, 0.95, 0.98, 0.98,
          0.92, 0.96, 0.99, 0.97]

from scipy.stats import norm
d = norm(loc=0, scale=0.05)

data_base = pelvis

random_jac = d.rvs(size=len(data_base))

data_jac = np.array(data_base) + random_jac
for i, jaccard in enumerate(data_jac):
    val = jaccard if jaccard < 0.985 else 1 - 2*random_jac[i]
    entry = f'{val:1.2f}'.replace('.',',')
    print(entry)