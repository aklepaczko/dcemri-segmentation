from nibabel import load


def read_nii_image(filename):
    img = load(filename)
    return img.get_data(), img.affine
