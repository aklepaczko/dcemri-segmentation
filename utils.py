from os.path import join, exists
from os import makedirs


def get_frame(patient_id: int, study_id: int) -> int:
    frame_choices = {(1, 1): 15, (1, 2): 17,
                     (2, 1): 13, (2, 2): 14,
                     (3, 1): 12, (3, 2): 12,
                     (4, 1): 13, (4, 2): 12,
                     (5, 1): 14, (5, 2): 14,
                     (6, 1): 13, (6, 2): 13,
                     (7, 1): 14, (7, 2): 15,
                     (8, 1): 13, (8, 2): 16,
                     (9, 1): 17, (9, 2): 16,
                     (10, 1): 14, (10, 2): 15}
    return frame_choices.get((patient_id, study_id))


def make_paths(patient_id: int, study_id: int):
    # root path
    root_folder = r'C:\Users\Artur Klepaczko\Documents\Science\KidneysData'

    # root path to image frames
    image_folder = join(root_folder, 'BergenImagesBSpline')

    # path to patient data
    patient_folder = join(image_folder, f'SG2_FF{patient_id:02d}')

    # path to patient study
    study_folder = join(patient_folder, f'Vol_{study_id:d}')

    # path to annotations
    roi_folder = join(root_folder, 'BergenAnnotations', f'SG2_FF{patient_id:02d}',
                      f'Vol_{study_id}_Annotations')

    # path to segmentation masks
    mask_folder = join(root_folder, 'KidneyNiftiROIs', f'SG2_FF{patient_id:02d}',
                       f'Roi{study_id}')

    arrays_folder = join(patient_folder, f'Vol_{study_id}_Arrays')
    if not exists(arrays_folder):
        makedirs(arrays_folder)

    # root path to manual rois
    manual_roi_folder = join(root_folder, 'KidneyTiffROIs', f'SG2_FF{patient_id:02d}', f'Roi{study_id:d}')

    path_dict = {'image_folder':    image_folder,
                 'patient_folder':  patient_folder,
                 'study_folder':    study_folder,
                 'roi_folder':      roi_folder,
                 'arrays_folder':   arrays_folder,
                 'manual_folder':   manual_roi_folder,
                 'mask_folder':     mask_folder}

    return path_dict
